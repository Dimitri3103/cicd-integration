"use client";

import { useState } from "react";

export default function Home() {
  const [todos, setTodos] = useState([]);
  const [newTodo, setNewTodo] = useState("");

  const addTodo = () => {
    if (newTodo.trim() !== "") {
      const updatedTodos = [...todos, newTodo];
      setTodos(updatedTodos as any);
      setNewTodo("");
    }
  };
  const deleteTodo = (index: any) => {
    const updatedTodos = [...todos];
    updatedTodos.splice(index, 1);
    setTodos(updatedTodos);
  };
  return (
    <div>
      <h1>TEST INTEGRATION CI/CD</h1>
      <br />

      <h2>Déploiement Réussi.</h2>
      
      <h2>Déploiement Réussi.</h2>
      <h2>Déploiement Réussi.</h2>
      <h2>Déploiement Réussi.</h2>
      <br />
      <div>
        <input
          type="text"
          value={newTodo}
          data-testid="todo-input"
          onChange={(e) => setNewTodo(e.target.value)}
        />
        <button onClick={addTodo} data-testid="add-todo">
          Add Todo
        </button>
        <ul data-testid="todo-list">
          {todos.map((todo, index) => (
            <li key={index}>{todo}</li>
          ))}
        </ul>
        {todos.map((todo, index) => (
          <div key={index}>
            <button
              onClick={() => deleteTodo(index)}
              data-testid={`delete-todo-${index}`}
            >
              Delete
            </button>
          </div>
        ))}
      </div>
    </div>
  );
}
